# twitter Broadcast

***run test***
```
python -m unittest discover -s tests/integration
```

***run application***
```
# it's required credentials using environment variables 
python streaming_server.py
```

***build docker***
```
docker build -t twitter-broadcast .
```

***run docker***
```
docker run -it --rm --name twitter-broadcast -h twitter-broadcast \
-p 9009:9009 \
-e ACCESS_TOKEN='YOUR_ACCESS_TOKEN' \
-e ACCESS_SECRET='YOUR_ACCESS_SECRET' \
-e CONSUMER_KEY='YOUR_CONSUMER_KEY' \
-e CONSUMER_SECRET='YOUR_CONSUMER_SECRET' \
-e TCP_IP='0.0.0.0' \
-e TCP_PORT=9009 \
-e QUERY_WORD=':)' \
twitter-broadcast
```