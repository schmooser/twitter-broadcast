FROM python:3.7.3

WORKDIR ./app

ENV ACCESS_TOKEN ''
ENV ACCESS_SECRET ''
ENV CONSUMER_KEY ''
ENV CONSUMER_SECRET ''
ENV TCP_IP 'localhost'
ENV TCP_PORT 9009
ENV QUERY_WORD ':)'

COPY ["requirements.txt", "streaming_server.py", "./"]

COPY twitter_broadcast ./twitter_broadcast

RUN pip install pip-tools && \
    pip-sync

EXPOSE 9009

CMD python streaming_server.py
