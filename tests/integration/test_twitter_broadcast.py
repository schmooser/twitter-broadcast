import unittest
from twitter_broadcast import TwitterBroadcast


class TestTwitterBroadcast(unittest.TestCase):
    def setUp(self):
        self.twt_broadcast = TwitterBroadcast()

    def test_get_message(self):
        message = self.twt_broadcast.get_message({
            "timestamp_ms": "1192295266080026624",
            "id_str": "1573100317865",
            "text": "Hi! I'm #raphael #silva #de #oliveira"
        })
        message_to_compare = "1192295266080026624\t1573100317865\tHi! I'm #raphael #silva #de #oliveira\n"

        self.assertEqual(message, message_to_compare)


if __name__ == '__main__':
    unittest.main()
