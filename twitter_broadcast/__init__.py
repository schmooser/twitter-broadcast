import json


class TwitterBroadcast:
    def parse_line(self, line):
        tweet = json.loads(line)
        return {
            "timestamp_ms": tweet['timestamp_ms'],
            "id_str": tweet['id_str'],
            "text": tweet['text']
        }

    def get_message(self, tweet):
        return "%s\t%s\t%s\n" % (
                                tweet.get("timestamp_ms"),
                                tweet.get("id_str"),
                                tweet.get("text").replace('"', ' ').replace('\t', ' ').replace('\n', ' ').replace('\r', '')
        )