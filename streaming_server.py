import requests
import requests_oauthlib
import socket
import os
import urllib.parse
import time
from twitter_broadcast import TwitterBroadcast

ACCESS_TOKEN = os.getenv('ACCESS_TOKEN', '')
ACCESS_SECRET = os.getenv('ACCESS_SECRET', '')
CONSUMER_KEY = os.getenv('CONSUMER_KEY', '')
CONSUMER_SECRET = os.getenv('CONSUMER_SECRET', '')
QUERY_WORD = os.getenv('QUERY_WORD', ':)')
twitter_auth = requests_oauthlib.OAuth1(CONSUMER_KEY, CONSUMER_SECRET, ACCESS_TOKEN, ACCESS_SECRET)

twt_broadcast = TwitterBroadcast()

TCP_IP = os.getenv('TCP_IP', "localhost")
TCP_PORT = int(os.getenv('TCP_PORT', "9009"))


def try_connect(socket_conn):
    socket_conn.connect((TCP_IP, TCP_PORT))


def get_tweets():
    url = 'https://stream.twitter.com/1.1/statuses/filter.json'
    query_data = [('language', 'en'), ('track', urllib.parse.quote(QUERY_WORD))]
    query_url = url + '?' + '&'.join([str(t[0]) + '=' + str(t[1]) for t in query_data])
    print(query_url)
    response = requests.get(query_url, auth=twitter_auth, stream=True)
    print(query_url, response)
    return response


def send_tweets(http_resp, tcp_connection):
    for line in http_resp.iter_lines():
        try:
            message = twt_broadcast.get_message(twt_broadcast.parse_line(line))
            print("Tweet: " + message)
            print("------------------------------------------")
            tcp_connection.sendall(message.encode('utf-8'))
        except BaseException as err:
            print("Error: %s" % str(err))
            tcp_connection.close()
            break


# put while with sleep in case of all clients disconnect
while 1:
    try:
        tcp_conn = None
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.bind((TCP_IP, TCP_PORT))
        s.listen(5)
        print("Waiting for TCP connection...")

        tcp_conn, addr = s.accept()
        resp = get_tweets()
        send_tweets(resp, tcp_conn)
        time.sleep(1)
    except TypeError as e:
        print(e)
        pass
